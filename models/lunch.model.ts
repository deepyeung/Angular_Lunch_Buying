
export class Lunch{
  id: number = 0;
  name: string = "";
  price: number = 0;
  count: string = "";
  description: string = "";
}
